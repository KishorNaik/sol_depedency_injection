﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Dependency_Injection
{
    class Program
    {
        static void Main(string[] args)
        {
            ShapeChoiceNonDi shapeNonDiObj = new ShapeChoiceNonDi();
            shapeNonDiObj.DrawChoice(ShapeChoiceNonDi.ShapeChoice.Circle);


            ShapeChoiceDi shapeObj = new ShapeChoiceDi(new CircleDi());
            shapeObj.DrawShape();

            ShapeChoiceDiProperty shapePropertyObj = new ShapeChoiceDiProperty()
            {
                Shape = new CircleDi()
            };
            shapePropertyObj.DrawShape();


            ShapeChoiceDiMethod shapeMethodObj = new ShapeChoiceDiMethod();
            shapeMethodObj.AddChoice(new CircleDi());
            shapeMethodObj.DrawShape();
            

        }
    }

    #region Non DI 
    public class ShapeNonDi
    {
        public void Draw()
        {
            System.Console.WriteLine("Draw Shape");
        }
    }

    public class CircleNonDi
    {
        public void Draw()
        {
            System.Console.WriteLine("Draw Cricle");
        }
    }

    public class RectangleNonDi
    {
        public void Draw()
        {
            System.Console.WriteLine("Draw Rectangle");
        }
    }

    public class ShapeChoiceNonDi
    {
        public enum ShapeChoice
        {
            Circle=0,
            Rectangle=1
        };

        public void DrawChoice(ShapeChoice shapeChoiceObj)
        {
            switch(shapeChoiceObj)
            {
                case ShapeChoice.Circle:
                    CircleNonDi circleNonDiObj = new CircleNonDi();
                    circleNonDiObj.Draw();
                    break;

                case ShapeChoice.Rectangle:
                    RectangleNonDi rectangleDiObj = new RectangleNonDi();
                    rectangleDiObj.Draw();
                    break;
            }
        }
    }
    #endregion

    #region DI 

   public interface IShapeDi
    {
        void Draw();
    }

    public class CircleDi : IShapeDi
    {
        public void Draw()
        {
            System.Console.WriteLine("Draw Cricle");
        }
    }

    public class RectangleDi : IShapeDi
    {
        public void Draw()
        {
            System.Console.WriteLine("Draw Rectangle");
        }
    }

    /// <summary>
    /// Constructor Injection
    /// </summary>
    public class ShapeChoiceDi
    {
        #region Declaration
        private IShapeDi shapeObj = null;
        #endregion

        #region Constructor
        public ShapeChoiceDi(IShapeDi shapeObj)
        {
            this.shapeObj = shapeObj;
        }
        #endregion

        #region Public Method
        public void DrawShape()
        {
            this.shapeObj.Draw();
        }
        #endregion
    }


    /// <summary>
    /// Property Injection
    /// </summary>
    public class ShapeChoiceDiProperty
    {
       
        #region Property
        public IShapeDi Shape { get; set; }
        #endregion

        #region Public Method
        public void DrawShape()
        {
            this.Shape.Draw();
        }
        #endregion
    }


    public class ShapeChoiceDiMethod
    {
        #region Declaration
        private IShapeDi shapeDiObj = null;
        #endregion

        #region Method
        public void AddChoice(IShapeDi shapeDiObj)
        {
            this.shapeDiObj = shapeDiObj;

            //shapeDiObj.Draw(); // Association but it is not DI
        }

        public void DrawShape()
        {
            this.shapeDiObj.Draw();
        }
        #endregion 
    }
    #endregion 


}
